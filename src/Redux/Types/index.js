// Error alerts
export const SHOW_ALERT = 'SHOW_ALERT';
export const HIDE_ALERT = 'HIDE_ALERT';

// Main information tab
export const UPDATE_CHANGE_EMAIL = 'UPDATE_CHANGE_EMAIL';
export const UPDATE_CHANGE_PASSWORD = 'UPDATE_CHANGE_PASSWORD';
export const UPDATE_CONFIRM_PASSWORD = 'UPDATE_CONFIRM_PASSWORD';


// Additional information tab
export const CHANGE_FIRST_NAME = 'CHANGE_FIRST_NAME';
export const CHANGE_LAST_NAME = 'CHANGE_LAST_NAME';
export const MODIFY_ADDRESS = 'MODIFY_ADDRESS';
export const CHANGE_YOUR_COUNTRY = 'CHANGE_YOUR_COUNTRY';