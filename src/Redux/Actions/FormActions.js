import {
    SHOW_ALERT,
    UPDATE_ADD_INFO,
    UPDATE_CHANGE_EMAIL,
    UPDATE_CONFIRM_PASSWORD,
    CHANGE_FIRST_NAME,
    CHANGE_LAST_NAME,
    MODIFY_ADDRESS,
    CHANGE_YOUR_COUNTRY
} from '../Types/index';

export function updateChangeEmail(UserInformation) {
    return (dispatch) => {
        dispatch(changeEmail());

    }
}

const changeEmail = (UserInformation) => ({
    type: UPDATE_CHANGE_EMAIL
})


// Show an alert
export function showAlert(UserInformation) {
    return (dispatch) => {
        dispatch(showErrorAlert());
    }
}

const showErrorAlert = () => ({
    type: SHOW_ALERT
})