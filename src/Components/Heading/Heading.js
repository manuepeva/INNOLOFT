import React from 'react'
import styled from '@emotion/styled';
import { css } from '@emotion/core';
import LogoImg from '../../Images/logo_innoloft.png';


// Header
const ContainerHeader = styled.div`
    display: flex !important;
    justify-content: space-between;
    width:100%;
    margin: 0 auto;
    background: linear-gradient(90deg, rgba(215,221,95,1) 0%, rgba(138,175,98,1) 35%, rgba(47,122,99,1) 100%); 
    @media(max-width: 768px){
        display: inline-flex !important;
        max-width: 100%;
        img{
            max-width:70%;
        }
    }
    .p{
        margin-top: 2rem;
    }
`;

const ItemsHead = styled.div`
    display: flex !important;
    color: #fff;
    i{
        padding: 2rem 2rem 0rem 2rem;
        font-size: .8rem;
    }
`;




const Heading = () => {
    return (
        <>
            <header >
                <ContainerHeader>
                    <div
                    >
                        <img src={LogoImg} alt="Logo of the website" />


                    </div>

                    <ItemsHead>
                        <div
                        >
                            <p
                            ><i className="tiny material-icons">language EN</i></p>




                        </div>
                        <div
                        >
                            <p
                            ><i className="tiny material-icons">mail</i></p>




                        </div>
                        <div
                        >
                            <p
                            ><i className="tiny material-icons">add_alert</i></p>




                        </div>
                    </ItemsHead>
                </ContainerHeader>
            </header>
        </>
    )
}

export default Heading;
